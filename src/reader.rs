use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use std::time::{Instant};

use crossbeam::channel;

use crate::{Config, Target};
use crate::bench_reader::BenchReader;

pub fn read(config: Arc<Config>, reader_input: channel::Sender<Vec<u8>>) {
    match &config.source {
        Target::TCP(address) => {
            read_socket(config.clone(), address, &reader_input).unwrap();

            if config.debug {
                println!("Finished reading TCP socket.");
            }
        },
        Target::UDP(address) => {
            panic!("wormhole-sender can't listen to UDP connections")
        },
        Target::File(path) => {
            read_file(config.clone(), path, &reader_input).unwrap();
        }
        Target::Bench => {
            read_bench(config.clone(), &reader_input).unwrap();
        }
    }
}

fn read_read(config: Arc<Config>, read: &mut dyn Read, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    let start = Instant::now();
    let mut total_bytes_read = 0;
    let read_len = config.packet_size * config.shards;
    loop {
        let mut buffer = Vec::with_capacity(read_len);
        buffer.resize(read_len, 0u8);
        match read.read(&mut buffer[0..read_len]) {
            Ok(0) => break,
            Ok(bytes_read) => {
                total_bytes_read += bytes_read;
                buffer.resize(bytes_read, 0u8);
                reader_input.send(buffer)?
            }
            Err(_) => {
                panic!("Failed to read from Reader.");
                // return Err(Box::new(e));
            }
        }
    }

    if config.debug {
        println!("[{}] Read {} bytes in {:?}", thread::current().name().unwrap(), total_bytes_read, start.elapsed());
    }

    Ok(())
}

fn read_file(config: Arc<Config>, path: &PathBuf, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        return Err(Box::<dyn Error>::from(format!("File `{:?}` does not exist.", path)));
    }

    if !path.is_file() {
        return Err(Box::<dyn Error>::from(format!("Path `{:?}` doesn't point to a file.", path)));
    }

    let mut file_handle = File::open(path)?;
    read_read(config, &mut file_handle, &reader_input)
}

fn read_socket(config: Arc<Config>, address: &SocketAddr, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    if config.debug {
        println!("Binding to `{}` to listen for connections.", address);
    }

    let listener = TcpListener::bind(address)?;
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                if config.debug {
                    println!("[{}] Received new connection. {:?}", thread::current().name().unwrap(), stream);
                }

                handle_client(config.clone(), &stream, reader_input);

                if config.debug {
                    println!("[{}] Closing connection with {:?}", thread::current().name().unwrap(), stream);
                }
            }
            Err(e) => return Err(Box::new(e))
        }
    }

    if config.debug {
        println!("[{}] Done listening to `{}` for connections.", thread::current().name().unwrap(), address);
    }

    Ok(())
}

fn read_bench(config: Arc<Config>, reader_input: &channel::Sender<Vec<u8>>) -> Result<(), Box<dyn Error>> {
    let mut bench_reader = BenchReader::new();
    read_read(config, &mut bench_reader, reader_input)
}

fn handle_client(config: Arc<Config>, mut stream: &TcpStream, reader_input: &channel::Sender<Vec<u8>>) {
    read_read(config, &mut stream, reader_input).unwrap();
}