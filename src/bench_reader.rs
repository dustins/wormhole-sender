use std::io::{self, Read};

pub struct BenchReader {}

impl BenchReader {
    pub fn new() -> BenchReader {
        BenchReader {}
    }
}

impl Read for BenchReader {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        Ok(buf.len())
    }
}