use std::sync::Arc;
use std::thread;

use crossbeam::channel;
use reed_solomon_erasure::galois_8::ReedSolomon;

use crate::Config;

pub fn encoder(config: Arc<Config>, reader_output: channel::Receiver<Vec<u8>>, sender_input: channel::Sender<Vec<u8>>) {
    for i in 0..config.encoder_threads {
        let config = config.clone();
        let reader_output = reader_output.clone();
        let sender_input = sender_input.clone();

        if config.debug {
            println!("Spawning [Encoder-{}]", i);
        }

        thread::Builder::new()
            .name(format!("Encoder-{}", i))
            .spawn(move || {
                let encoder = ReedSolomon::new(config.shards, config.parity).unwrap();
                for mut buffer in reader_output {
                    if config.verbose > 1 {
                        println!("[{}] Received buffer {:?}", thread::current().name().unwrap(), buffer);
                    }

                    buffer.resize(config.packet_size * config.total_shard_count(), 0u8);
                    let chunks: Vec<&mut [u8]> = buffer.chunks_mut(config.packet_size).collect();
                    encoder.encode(chunks).unwrap();

                    sender_input.send(buffer).expect("Failed to send buffer to Sender");
                }

                if config.debug {
                    println!("[{}] Shutting down", thread::current().name().unwrap());
                }
            })
            .expect("Failed to spawn thread");
    }
}