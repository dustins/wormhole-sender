use std::error::Error;
use std::sync::Arc;
use std::thread;

use byteorder::{BigEndian, WriteBytesExt, LittleEndian};

use crossbeam::channel;
use crossbeam::channel::select;

pub use crate::config::*;
use std::net::{UdpSocket, TcpStream};
use std::io::Write;
use std::time::Duration;
use std::fs::File;

mod encoder;

mod bench_reader;

mod reader;
mod config;

pub fn run(config: Arc<Config>) -> Result<(), Box<dyn Error>> {
    // setup signaling channels
    let ctrl_c_events = ctrl_channel().expect("Unable to get Ctrl-c channel");

    // setup data flow channels
    let (reader_input, reader_output) = channel::bounded(READER_CHANNEL_SIZE);
    let (sender_input, encoder_output) = channel::bounded(SENDER_CHANNEL_SIZE);

    spawn_reader(config.clone(), reader_input);
    spawn_encoder(config.clone(), reader_output, sender_input);
    let sender_done = spawn_sender(config.clone(), encoder_output);

    let mut total_bytes_sent = 0;
    loop {
        select! {
            recv(ctrl_c_events) -> _ => {
                if config.debug {
                    println!("Received signal of Ctrl-c");
                }

                break;
            }
            recv(sender_done) -> result => {
                match result {
                    Ok(bytes_sent) => {
                        total_bytes_sent += bytes_sent;
                    },
                    Err(_) => {
                        if config.debug {
                            println!("[{}] Received signal all senders completed", thread::current().name().unwrap());
                            println!("[{}] Sent {} bytes", thread::current().name().unwrap(), total_bytes_sent);
                        }

                        break;
                    }
                }
            }
        }
    }

    Ok(())
}

fn spawn_reader(config: Arc<Config>, reader_input: channel::Sender<Vec<u8>>) {
    if config.debug {
        println!("Spawning [Reader]");
    }

    // todo decide how to handle multiple reader threads
    thread::Builder::new()
        .name("Reader".to_string())
        .spawn(move || {
            reader::read(config.clone(), reader_input);

            if config.debug {
                println!("[{}] Shutting down", thread::current().name().unwrap());
            }
        }).expect("Failed to spawn thread.");
}

fn spawn_encoder(config: Arc<Config>, reader_output: channel::Receiver<Vec<u8>>, sender_input: channel::Sender<Vec<u8>>) {
    encoder::encoder(config.clone(), reader_output, sender_input);
}

fn spawn_sender(config: Arc<Config>, encoder_output: channel::Receiver<Vec<u8>>) -> channel::Receiver<usize> {
    let (sender_done_tx, sender_done_rx) = channel::bounded(10);

    for i in 0..config.sender_threads {
        let config = config.clone();
        let encoder_output = encoder_output.clone();
        let sender_done_tx = sender_done_tx.clone();

        if config.debug {
            println!("Spawning [Sender-{}]", i);
        }

        thread::Builder::new()
            .name(format!("Sender-{}", i))
            .spawn(move || {
                let mut total_bytes_sent = 0;
                let mut packet_number = 0u32;
                let mut send_buffer = Vec::with_capacity(9000);

                match &config.target {
                    Target::File(path) => {
                        let mut handle = File::create(path).unwrap();
                        for buffer in encoder_output {
                            if config.verbose > 1 {
                                println!("[{}] Received buffer", thread::current().name().unwrap());
                            }

                            buffer.chunks(config.packet_size).for_each(|packet| {
                                send_buffer.resize(0, 0u8);
                                send_buffer.write_u16::<LittleEndian>(packet.len() as u16);
                                send_buffer.write_u32::<LittleEndian>(packet_number);
                                send_buffer.write(&packet);
                                match handle.write(&send_buffer) {
                                    Ok(bytes_sent) => {
                                        packet_number += 1;
                                        total_bytes_sent += bytes_sent;
                                    }
                                    Err(e) => panic!("Unable to send. {}", e)
                                }
                            });
                        }
                    }
                    Target::UDP(address) => {
                        let sock = UdpSocket::bind("0.0.0.0:0").unwrap();
                        sock.set_broadcast(true).unwrap();
                        sock.connect(address).unwrap();

                        for buffer in encoder_output {
                            if config.verbose > 1 {
                                println!("[{}] Received buffer", thread::current().name().unwrap());
                            }

                            buffer.chunks(config.packet_size).for_each(|packet| {
                                send_buffer.resize(0, 0u8);
                                send_buffer.write_u16::<LittleEndian>(buffer.len() as u16);
                                send_buffer.write_u32::<LittleEndian>(packet_number);
                                send_buffer.write(&buffer);
                                match sock.send(&send_buffer) {
                                    Ok(bytes_sent) => {
                                        packet_number += 1;
                                        total_bytes_sent += bytes_sent;
                                    }
                                    Err(e) => panic!("Unable to send. {}", e)
                                }
                            });
                        }
                    }
                    Target::TCP(address) => {
                        let mut sock = TcpStream::connect("127.0.0.1:4488").unwrap();

                        for buffer in encoder_output {
                            if config.verbose > 1 {
                                println!("[{}] Received buffer", thread::current().name().unwrap());
                            }

                            buffer.chunks(config.packet_size).for_each(|packet| {
                                send_buffer.resize(0, 0u8);
                                send_buffer.write_u32::<LittleEndian>(packet_number);
                                send_buffer.write_u16::<LittleEndian>(buffer.len() as u16);
                                send_buffer.write(&buffer);
                                match sock.write(&send_buffer) {
                                    Ok(bytes_sent) => {
                                        packet_number += 1;
                                        total_bytes_sent += bytes_sent;
                                    }
                                    Err(e) => panic!("Unable to send. {}", e)
                                }
                            });
                        }
                    }
                    t => panic!("wormhole-sender doesn't support sending to {:?}", t)
                }

                if config.debug {
                    println!("[{}] Shutting down", thread::current().name().unwrap());
                    println!("[{}] Sent {} bytes", thread::current().name().unwrap(), total_bytes_sent);
                }

                sender_done_tx.send(total_bytes_sent).unwrap();
            }).expect("Failed to spawn thread.");
    }

    sender_done_rx
}

/// Returns a channel that fires when Ctrl-C is pressed
fn ctrl_channel() -> Result<crossbeam::Receiver<()>, ctrlc::Error> {
    let (sender, receiver) = crossbeam::bounded(10);
    ctrlc::set_handler(move || {
        println!("Ctrl-c pressed. Exiting.");
        let _ = sender.send(());
    })?;

    Ok(receiver)
}