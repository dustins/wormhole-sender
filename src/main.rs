use std::sync::Arc;

use structopt::StructOpt;

use wormhole_sender::{Config, run};
use std::time::Instant;

fn main() {
    let config: Arc<Config> = Arc::new(Config::from_args());
    if config.debug {
        println!("{:?}", config);
    }

    let start = match config.debug {
        true => Some(Instant::now()),
        false => None
    };

    if let Err(e) = run(config) {
        eprintln!("{}", e);
    }

    if let Some(start) = start {
        println!("Ran in {:?}", start.elapsed());
    }

    println!("Done.");
}
